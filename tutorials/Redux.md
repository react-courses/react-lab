# Redux

## Instalación

-   Instalar <strong>[Redux DevTools](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd/related?hl=es)</strong>
-   Instalar Redux con `yarn add redux`

---

## 3 Principios

1. <b>Única fuente de la verdad:</b> El estado completo de tu aplicación debe ser representado en <b>un solo Objeto de Javascript</b> dentro de <b>un solo store</b>.

2. <b>El estado es de solo lectura:</b> la única forma de modificar el estado es mediante despachar una <b>acción</b>, la acción es un objeto que describe de forma mínima, que cambia en la aplicación.

3. <b>Los cambios se hacen con Funciones Puras:</b> Para espoecificar mutaciones en el estado debemos escribir funciones puras. Es decir, debemos tomar el estado previo de la aplicación, la acción que es "despachada" y regresar el <b>nuevo</b> objeto estado. <b>A esta función, se le llama Reducer.</b>

---

## Stores

-   Contiene el estado de la aplicación.
-   Puede acceder al estado mediante `getState()`

Import:

```javascript
import { createStorage } from "redux";
```

Create:

```javascript
/**
 *
 * Code for Redux store creation
 *
 */
```
