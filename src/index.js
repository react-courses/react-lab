/**
 * Tutorial_1: Install "redux" and "react-redux".
 * Tutorial_1: import {createStore} from"redux"
 * Tutorial_1: create our rootReducer.
 **/

import "./app/styles/index.scss";

import React from "react";
import { render } from "react-dom";
import { createStore } from "redux";
import rootReducer from "./app/reducers";
import Root from "./app/components/Root";

const store = createStore(rootReducer);
render(<Root store={store} />, document.getElementById("root"));
