/**
 * Tutorial_1: import Provider from"react-redux"
 */
import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import routes from "../routes";
import Layout from "./layout/Layout";
import Error from "../screens/ErrorScreen";

class App extends Component {
    render() {
        return (
            <Layout>
                <Switch>
                    {routes.map(route => {
                        return (
                            <Route
                                key={route.path}
                                path={route.path}
                                exact={route.exact}
                                component={route.component}
                            />
                        );
                    })}
                    <Route component={Error} />
                </Switch>
            </Layout>
        );
    }
}

export default App;
