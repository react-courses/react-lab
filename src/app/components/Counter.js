import React, { Component } from "react";
import PropTypes from "prop-types";

export default class Counter extends Component {
    render() {
        let { value, onIncrement, onDecrement, index } = this.props;
        return (
            <div className="card" style={{ marginBottom: 30 }}>
                <header className="card-header">
                    <p className="card-header-title">Contador {index}</p>
                </header>
                <div className="card-content">
                    <p className="title has-text-centered">{value}</p>
                </div>
                <footer className="card-footer">
                    <button
                        onClick={onDecrement}
                        className="button is-white is-large card-footer-item is-size-3"
                    >
                        -
                    </button>
                    <button
                        onClick={onIncrement}
                        className="button is-light is-large card-footer-item is-size-3"
                    >
                        +
                    </button>
                </footer>
            </div>
        );
    }
}

Counter.propTypes = {
    index: PropTypes.number.isRequired,
    value: PropTypes.number,
    onIncrement: PropTypes.func.isRequired,
    onDecrement: PropTypes.func.isRequired,
};

Counter.defaultProps = {
    value: 100,
};
