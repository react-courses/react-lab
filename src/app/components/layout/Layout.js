import React, { Component } from "react";
import Navigation from "./Navigation";
import Footer from "./Footer";
import LoginModal from "./LoginModal";

export default class Layout extends Component {
    state = {
        isModalOpen: false,
    };

    toggleModal = event => {
        event.preventDefault();
        this.setState(prevState => {
            return {
                isModalOpen: !prevState.isModalOpen,
            };
        });
    };

    render() {
        return (
            <div className="Layout">
                <Navigation toggleModal={this.toggleModal} />
                <LoginModal
                    isActive={this.state.isModalOpen}
                    toggleModal={this.toggleModal}
                />
                <section className="section">
                    <div className="container">
                        <div className="content">{this.props.children}</div>
                    </div>
                </section>
                <Footer />
            </div>
        );
    }
}
