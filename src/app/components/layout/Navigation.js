import React, { Component } from "react";
import { Link } from "react-router-dom";
import routes from "../../routes";

export default class Navigation extends Component {
    constructor(props) {
        super();
        this.props = props;
        this.state = {
            isModalOpen: false,
        };
    }

    renderLinks() {
        return routes.map(route => {
            return (
                <Link
                    key={route.path}
                    className="navbar-item"
                    to={{ pathname: route.path, search: route.search }}
                >
                    {route.title}
                </Link>
            );
        });
    }

    render() {
        return (
            <nav
                className="navbar is-dark"
                role="navigation"
                aria-label="main navigation"
            >
                <div className="navbar-start">{this.renderLinks()}</div>

                <div className="navbar-end">
                    <div className="navbar-item">
                        <div className="buttons">
                            <button
                                onClick={this.props.toggleModal}
                                className="button is-primary is-rounded"
                            >
                                <strong>Ingresar</strong>
                            </button>
                        </div>
                    </div>
                </div>
            </nav>
        );
    }
}
