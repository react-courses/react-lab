import React, { Component } from "react";

export default class Navigation extends Component {
    render() {
        return (
            <footer className="footer">
                <div className="content has-text-centered">
                    <p>
                        <strong>React Lab</strong> - {new Date().getFullYear()}
                    </p>
                </div>
            </footer>
        );
    }
}
