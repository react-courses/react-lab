import React, { Component } from "react";

export default class LoginModal extends Component {
    render() {
        return (
            <div
                className={"modal" + (this.props.isActive ? " is-active" : "")}
            >
                <div className="modal-background" />
                <div className="modal-content" />
                <div className="modal-card">
                    <header className="modal-card-head">
                        <p className="modal-card-title">Ingresar</p>
                        <button
                            onClick={this.props.toggleModal}
                            className="delete"
                            aria-label="close"
                        />
                    </header>
                    <section className="modal-card-body">
                        <div className="field">
                            <label className="label">Correo Electrónico</label>
                            <div className="control">
                                <input
                                    className="input"
                                    type="mail"
                                    placeholder="example@mail.com"
                                />
                            </div>
                        </div>
                        <div className="field">
                            <label className="label">Contraseña</label>
                            <div className="control">
                                <input
                                    className="input"
                                    type="password"
                                    placeholder="*******"
                                />
                            </div>
                        </div>
                    </section>
                    <footer className="modal-card-foot">
                        <div className="field is-grouped is-grouped-right">
                            <p className="control">
                                <button
                                    onClick={this.onSubmit}
                                    className={"button is-primary"}
                                >
                                    Enviar
                                </button>
                            </p>
                            <p className="control">
                                <button
                                    onClick={this.props.toggleModal}
                                    className="button is-light"
                                >
                                    Cancelar
                                </button>
                            </p>
                        </div>
                    </footer>
                </div>
            </div>
        );
    }
}
