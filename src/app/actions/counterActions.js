/**
 * Tutorial_2: Create our constants
 * Tutorial_2: declare our "actions"
 */
import {
    INCREMENT,
    DECREMENT,
    ADD_COUNTER,
    REMOVE_COUNTER,
} from "../constants/actionTypes";

// Action Creator
export function addCounter() {
    // Action
    return {
        type: ADD_COUNTER,
    };
}

// Action Creator
export function removeCounter() {
    // Action
    return {
        type: REMOVE_COUNTER,
    };
}

// Action Creator
export function incrementCounter(_idx) {
    // Action
    return {
        type: INCREMENT,
        index: _idx,
    };
}

// Action Creator
export function decrementCounter(_idx) {
    // Action
    return {
        type: DECREMENT,
        index: _idx,
    };
}
