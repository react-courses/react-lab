import React, { Component } from "react";

export default class ErrorScreen extends Component {
    render() {
        return (
            <div className="ErrorScreen">
                <h1>404</h1>
                <p>¡Oooops!, No existe esa ruta.</p>
            </div>
        );
    }
}
