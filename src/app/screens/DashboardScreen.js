import React, { Component } from "react";

export default class DashboardScreen extends Component {
    get nameParam() {
        let params = new URLSearchParams(this.props.location.search);
        return params.get("name");
    }

    render() {
        return (
            <div className="DashboardScreen">
                <h1>Dasboard</h1>
                <p>El nombre que buscaste fue: {this.nameParam}</p>
            </div>
        );
    }
}
