/**
 * Tutorial_3: import connect in the Container component from "react-redux".
 * Tutorial_3: import actions in the Container component. (¿Why?)
 */
import React, { Component } from "react";
import Counter from "../components/Counter";
import { connect } from "react-redux";
import {
    addCounter,
    removeCounter,
    incrementCounter,
    decrementCounter,
} from "../actions/counterActions";

class CounterScreen extends Component {
    renderCounters = () => {
        return this.props.counters.map((item, index) => {
            return (
                <Counter
                    key={`counter-${index}`}
                    index={index}
                    value={item}
                    onIncrement={() => this.onIncrement(index)}
                    onDecrement={() => this.onDecrement(index)}
                />
            );
        });
    };

    onAddCounter = () => {
        this.props.dispatch(addCounter());
    };

    onRemoveCounter = () => {
        this.props.dispatch(removeCounter());
    };

    onIncrement = _idx => {
        this.props.dispatch(incrementCounter(_idx));
    };

    onDecrement = _idx => {
        this.props.dispatch(decrementCounter(_idx));
    };

    render() {
        return (
            <div className="CounterScreen">
                <div className="level">
                    <h1 className="level-left">Contadores</h1>
                    <div className="buttons level-right has-addons">
                        <button
                            className={"button"}
                            onClick={() => this.onAddCounter()}
                        >
                            Add Counter
                        </button>
                        <button
                            className={"button"}
                            onClick={() => this.onRemoveCounter()}
                        >
                            Remove Counter
                        </button>
                    </div>
                </div>

                {this.renderCounters()}
            </div>
        );
    }
}
const mapStateToProps = function(state) {
    return {
        ...state.counters,
    };
};

export default connect(mapStateToProps)(CounterScreen);
