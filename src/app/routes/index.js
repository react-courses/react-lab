import HomeScreen from "../screens/HomeScreen";
import AboutScreen from "../screens/AboutScreen";
import CounterScreen from "../screens/CounterScreen";
import DashboardScreen from "../screens/DashboardScreen";

export default [
    {
        path: "/",
        exact: true,
        title: "Inicio",
        component: HomeScreen,
    },
    {
        path: "/nosotros",
        title: "Nosotros",
        component: AboutScreen,
    },
    {
        path: "/dashboard",
        title: "Dashboard",
        search: "?name=jose",
        component: DashboardScreen,
    },
    {
        path: "/contador",
        title: "Contadores",
        component: CounterScreen,
    },
];
