import counter from "../../reducers/Counter";

test("Counter: INCREMENT", () => {
    expect(counter(0, { type: "INCREMENT" })).toBe(1);
});

test("Counter: DECREMENT", () => {
    expect(counter(1, { type: "DECREMENT" })).toBe(0);
});

test("Counter: UNKNOWN", () => {
    expect(counter(0, { type: "UNKNOWN" })).toBe(0);
});
