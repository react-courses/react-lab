/**
 *
 * Reducers never:
 *  - Mutate its arguments
 *  - NO API Calls
 *  - Call non Pure Functions
 *
 * Reducers allway:
 *  - Returns new state object
 *  - JUST calculate the next state
 *
 * Tutorial_3:Specify Initial state.
 */
import {
    INCREMENT,
    DECREMENT,
    ADD_COUNTER,
    REMOVE_COUNTER,
} from "../constants/actionTypes";

const initialState = {
    counters: [0],
};

export function counters(state = initialState, action) {
    const { counters } = state;

    switch (action.type) {
        case ADD_COUNTER:
            return { ...state, counters: [...counters, 0] };
        case REMOVE_COUNTER:
            return {
                ...state,
                counters: [...counters.slice(0, -1)],
            };
        case INCREMENT:
            return {
                ...state,
                counters: [
                    ...counters.slice(0, action.index),
                    counters[action.index] + 1,
                    ...counters.slice(action.index + 1),
                ],
            };
        case DECREMENT:
            return {
                ...state,
                counters: [
                    ...counters.slice(0, action.index),
                    counters[action.index] - 1,
                    ...counters.slice(action.index + 1),
                ],
            };
        default:
            return state;
    }
}
