// TODO: EXPORT MAIN REDUCER WITH (COMBINE REDUCERS I THINK)
/**
 * Tutorial_2: get to know whats is "combine reducers".
 * Tutorial_2: Create and import counters;
 */

import { combineReducers } from "redux";
import { counters } from "./counterReducers";

export default combineReducers({
    counters,
});
