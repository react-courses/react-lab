# React Lab

Created with: create-react-app

### Run project:

```yarn start``` or with npm ```npm start```

### Includes:

-   [React Router](https://reacttraining.com/react-router/web/guides/quick-start)
-   [Redux](https://redux.js.org)
